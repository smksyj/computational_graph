from lexer import Lexer
from parser import Parser
import sys
import cg_utils

if __name__=='__main__':
    lexer = Lexer()
    bnf_file_name = 'bnf.txt'
    parser = Parser(bnf_file_name)
    sys.argv.append('resolve_input.txt')

    if len(sys.argv) > 1:
        input_file_name = sys.argv[1]
        lines = map(lambda line: line.replace('\n', ''),
                    open(input_file_name, 'r').readlines())

        for line in lines:
            if cg_utils.invalid_line(line):
                continue
            line, var_assign, der_names = cg_utils.split_line(line)
            print(line, var_assign, der_names)
            tokens = lexer.tokenize(line)
            graph = parser.parse(tokens)
            print('graph: ', graph)
            print('resolve: ', graph.resolve())
            print()
