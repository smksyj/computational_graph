from symbols import SYMBOLS
import math

class CNode(object):
    def __init__(self, token_type, buf=None):
        if buf:
            self.raw_string = ''.join(buf)
        else:
            self.raw_string = ''
        self.token_type = token_type
        self.token_str = str(token_type).split('.')[1]
        self.children = []

    def __str__(self):
        return self.raw_string + ':' + str(self.token_type).split('.')[1]

    def __repr__(self):
        return self.__str__()

    def children_str(self):
        s = '[' + self.token_str + ']'
        t = ''
        for child in self.children:
            child_str = child.children_str()
            t += child_str

        if len(t) == 0:
            return s
        else:
            return '[{}-{}]'.format(s, t)

    def evaluate(self, var_assign=None):
        raise NotImplementedError('Override this method')

    def calculate(self, parent_value, var_assign=None):
        raise NotImplementedError('Override this method')

class StackTopNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.STACK_TOP, buf)

class EpsilonNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.epsilon, buf)

class LParenNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.L_PAREN, buf)

class RParenNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.R_PAREN, buf)

class PlusNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.OPER_PLUS, buf)

    def op(self):
        return lambda a, b: a + b

    def to_string(self):
        return '+'

class MinusNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.OPER_MIN, buf)

    def op(self):
        return lambda a, b: a - b

    def to_string(self):
        return '-'

class MultNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.OPER_MULT, buf)

    def op(self):
        return lambda a, b: a * b

    def to_string(self):
        return '*'

class DivNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.OPER_DIV, buf)

    def op(self):
        return lambda a, b: a / b

    def to_string(self):
        return '/'

class CommaNode(CNode):
    def __init__(self, buf=None):
        super().__init__(SYMBOLS.COMMA, buf)

class ConstNode(CNode):
    def __init__(self, buf=None):
        s = ''.join(buf)
        super().__init__(SYMBOLS.CONST, s)

    def evaluate(self, var_assign=None):
        return self.children[0].evaluate()

    def to_string(self):
        return self.children[0].to_string()

    @classmethod
    def from_number(cls, num):
        c = ConstNode('CONST')
        if num == 'E':
            c.children.append(ENode('E'))
            return c
        elif num == 'PI':
            c.children.append(PINode('PI'))
            return c
        else:
            raise Exception('wrong CONST type')

class ENode(CNode):
    def __init__(self, buf=None):
        s = ''.join(buf)
        self.value = math.e
        super().__init__(SYMBOLS.E, s)

    def evaluate(self, var_assign=None):
        return self.value

    def to_string(self):
        return 'E'

class PINode(CNode):
    def __init__(self, buf=None):
        s = ''.join(buf)
        self.value = math.pi
        super().__init__(SYMBOLS.PI, s)

    def evaluate(self, var_assign=None):
        return self.value

    def to_string(self):
        return 'PI'

class NumberNode(CNode):
    def __init__(self, buf=None):
        if isinstance(buf, str):
            super().__init__(SYMBOLS.NUMBER, [buf])
        elif isinstance(buf, float) or isinstance(buf, int):
            super().__init__(SYMBOLS.NUMBER, [str(buf)])
        else:
            super().__init__(SYMBOLS.NUMBER, buf)
        if buf is None:
            self.value = 0
        else:
            self.value = float(self.raw_string)

    def evaluate(self, var_assign=None):
        return self.value

    def children_str(self):
        return '[' + self.token_str + ':' + str(self.value) + ']'

    def to_string(self):
        return str(self.value)

class VariableNode(CNode):
    def __init__(self, buf=None):
        super().__init__(SYMBOLS.VARIABLE, buf)
        self.value = ''.join(buf)

    def children_str(self):
        return '[' + self.token_str + ':' + str(self.value) + ']'

    def evaluate(self, var_assign=None):
        if var_assign is None or self.value not in var_assign:
            raise Exception('Unassigned Variable: {}'.format(self.value))
        else:
            return var_assign[self.value]

    def to_string(self):
        return self.value

    def has_variable_name(self, var_name):
        return self.value == var_name