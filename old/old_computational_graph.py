import math
from symbols import SYMBOLS


class CNode(object):
    def __init__(self, token_type, buf=None):
        if buf:
            self.raw_string = ''.join(buf)
        else:
            self.raw_string = ''
        self.token_type = token_type
        self.token_str = str(token_type).split('.')[1]
        self.children = []

    def __str__(self):
        return self.raw_string + ':' + str(self.token_type).split('.')[1]

    def __repr__(self):
        return self.__str__()

    def evaluate(self, var_assign=None):
        raise NotImplementedError('Override this method')

    def derivative(self, variable_name, parent_vars=None):
        raise NotImplementedError('derivate_by')

    def children_str(self):
        s = '[' + self.token_str + ']'
        t = ''
        for child in self.children:
            child_str = child.children_str()
            t += child_str

        if len(t) == 0:
            return s
        else:
            return '[' + s + '-' + t + ']'

    def reform_graph(self, parent_vars):
        raise NotImplementedError('reform_graph: override this method')

    def calculate(self, parent_value, var_assign=None):
        raise NotImplementedError('Override this method')


class FunctionNode(CNode):
    def __init__(self, buf=None):
        super().__init__(SYMBOLS.FUNC, buf)
        
    def evaluate(self, var_assign=None):
        # FUNC::=FACTOR EXPR
        if self.children[0].token_type == SYMBOLS.FACTOR:
            l_val = self.children[0].evaluate(var_assign)
            result = self.children[1].calculate(l_val, var_assign)
            return result
        # FUNC::=OPERAND EXPR
        elif self.children[0].token_type == SYMBOLS.OPERAND:
            # l_val = self.children[0].evaluate(var_assign)
            # result = self.children[1].calculate(l_val, var_assign)
            # return result

            if self.children[1].children[0].token_type == SYMBOLS.OPER_MDP:
                l_val = self.children[0].evaluate(var_assign)
                result = self.children[1].calculate(l_val, var_assign)
                return result
            elif self.children[1].children[0].token_type == SYMBOLS.TERM: # EXPR -> OPER_PM FUNC
                l_val = self.children[0].evaluate(var_assign)
                op = self.children[1].children[0].op()

                # r_l_val = self.children[1].left()
                # current_val = op(l_val, r_l_val)
                # result = self.children[1].calculate(current_val, var_assign)

                # r_val = self.children[1].evaluate(var_assign)
                # result = op(l_val, r_val)

                result = self.children[1].calculate(l_val, var_assign)

                return result
            elif self.children[1].children[0].token_type == SYMBOLS.epsilon:
                return self.children[0].evaluate(var_assign)
            else:
                raise Exception('FunctionNode - evaluate: child expr token type error {}'.format(self.children[1].children[0].token_type))
        # FUNC::=S_PARAM_FUNC L_PAREN FUNC R_PAREN EXPR
        elif self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:
            val = self.calculate_single_param_func(var_assign)
            result = self.children[4].calculate(val, var_assign)
            return result
        # FUNC::=LOG L_PAREN FUNC COMMA FUNC R_PAREN EXPR
        elif self.children[0].token_type == SYMBOLS.LOG:
            val = self.calculate_single_param_func(var_assign)
            result = self.children[6].calculate(val, var_assign)
            return result
        
        elif self.children[0].token_type == SYMBOLS.OPER_PM:
            # val = self.children[1].evaluate(var_assign)
            # return -val
            left = self.children[1].left()
            result = self.children[1].calculate(-left, var_assign)
            return result
        else:
            raise Exception('{} bnf expansion error'.format(self.token_str))

    def calculate(self, parent_value, var_assign=None):
        # called when (EXPR call)EXPR::=OPER_MDP FUNC or (TERM call)TERM::=OPER_PM FUNC case
        if self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:
            # return parent_value
            result = self.children[4].calculate(parent_value, var_assign)
            return result
        elif self.children[0].token_type == SYMBOLS.LOG:
            result = self.children[6].calculate(parent_value, var_assign)
            return result
        op = self.children[1].op()
        if op is None: # FUNC::=OPERAND EXPR -> EXPR is epsilon
            return parent_value
        else:
            r_value = self.children[1].evaluate(var_assign)
            result = op(parent_value, r_value)
            return result

    def left(self, var_assign=None):
        if self.children[0].token_type == SYMBOLS.FACTOR:
            return self.children[0].left(var_assign)
        elif self.children[0].token_type == SYMBOLS.OPERAND:
            return self.children[0].left(var_assign)
        elif self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:
            return self.calculate_single_param_func(var_assign)
        elif self.children[0].token_type == SYMBOLS.LOG:
            return self.calculate_single_param_func(var_assign)

    def calculate_single_param_func(self, var_assign=None):
        if self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:
            op = self.children[0].op()
            val = self.children[2].evaluate(var_assign)
            return op(val)
        elif self.children[0].token_type == SYMBOLS.LOG:
            base = self.children[2].evaluate(var_assign)
            x = self.children[4].evaluate(var_assign)
            return math.log(x, base)

    def reform_graph(self, parent_vars):
        # FUNC::=FACTOR EXPR|OPERAND EXPR|S_PARAM_FUNC L_PAREN FUNC R_PAREN EXPR|LOG L_PAREN FUNC COMMA FUNC R_PAREN EXPR
        if self.children[0].token_type == SYMBOLS.FACTOR: # FACTOR EXPR
            _expr = self.children[1]
            new_right = _expr.reform_graph(parent_vars)
            if new_right is None:
                new_self = self.children[0].reform_graph(parent_vars)
            else:
                new_self = _expr.op()
                new_left = self.children[0].reform_graph(parent_vars)
                new_self.children = [new_left, new_right]
            return new_self
        elif self.children[0].token_type == SYMBOLS.OPERAND: # OPERAND EXPR
            _expr = self.children[1]
            new_right = _expr.reform_graph(parent_vars)
            if new_right is None:
                new_self = self
            else:
                new_self = _expr.op()
                new_self.children = [self.children[0], new_right]
            return new_self
        elif self.children[0].token_type == SYMBOLS.S_PARAM_FUNC: # S_FUNC(...) EXPR
            _s_func = self.children[0]
            _s_func.children = [self.children[2].reform_graph(parent_vars)]
            _expr = self.children[4]
            new_right = _expr.reform_graph(parent_vars)
            if new_right is None:
                new_self = _s_func
            else:
                new_self = _expr.op()
                new_self.children = [_s_func, new_right]
            return new_self
        elif self.children[0].token_type == SYMBOLS.LOG: # LOG(...) EXPR
            _log = self.children[0]
            _log.children = [self.children[2].reform_graph(parent_vars), self.children[4].reform_graph(parent_vars)]
            _expr = self.children[6]
            new_right = _expr.reform_graph(parent_vars)
            if new_right is None:
                new_self = _log
            else:
                new_self = _expr.op()
                new_self.children = [_log, new_right]
            return new_self

    def derivative(self, variable_name, parent_vars=None):
        if parent_vars is not None: # called from EXPR::=OPER_MDP FUNC

            if self.children[0].token_type == SYMBOLS.FACTOR:

                raise NotImplementedError()
            elif self.children[0].token_type == SYMBOLS.OPERAND:
                self.children[0].derivative(variable_name, parent_vars)
                raise NotImplementedError()
            elif self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:
                raise NotImplementedError()
            elif self.children[0].token_type == SYMBOLS.LOG:
                raise NotImplementedError()
            else:
                raise NotImplementedError()

        else: # called from TERM::=OPER_PM FUNC
            # FUNC::=FACTOR EXPR|OPERAND EXPR|S_PARAM_FUNC L_PAREN FUNC R_PAREN EXPR|LOG L_PAREN FUNC COMMA FUNC R_PAREN EXPR
            if self.children[0].token_type == SYMBOLS.FACTOR:
                parent_vars = self.children[0].derivative(variable_name, parent_vars)
                result = self.children[1].derivative(variable_name, parent_vars)
                return result
            elif self.children[0].token_type == SYMBOLS.OPERAND:
                parent_vars = {self.children[0].value(): self.children[0]}
                result = self.children[1].derivative(variable_name, parent_vars)
                return result
            elif self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:

                op = self.children[0].derivative(variable_name)
                parent_vars = None


                raise NotImplementedError()
            elif self.children[0].token_type == SYMBOLS.LOG:
                raise NotImplementedError()
            else:
                raise NotImplementedError()


class OperandNode(CNode):
    def __init__(self, buf=None):
        super().__init__(SYMBOLS.OPERAND, buf)

    def evaluate(self, var_assign=None):
        return self.children[0].evaluate(var_assign)

    def left(self, var_assign=None):
        return self.children[0].evaluate(var_assign)


# TERMINAL OPERATOR NODE
class OperatorNode(CNode):
    def __init__(self, buf=None):
        s = ''.join(buf)
        self.value = s
        if s == '+' or s == 'OPER_PLUS':
            super().__init__(SYMBOLS.OPER_PLUS, s)
        elif s == '-' or s == 'OPER_MIN':
            super().__init__(SYMBOLS.OPER_MIN, s)
        elif s == '*' or s == 'OPER_MULT':
            super().__init__(SYMBOLS.OPER_MULT, s)
        elif s == '/' or s == 'OPER_DIV':
            super().__init__(SYMBOLS.OPER_DIV, s)
        elif s == '^' or s == 'OPER_POW':
            super().__init__(SYMBOLS.OPER_POW, s)

    def evaluate(self, var_assign=None):
        return self.value

    def derivative(self, variable_name, parent_vars=None):
        return None

    def reform_graph(self, parent_vars):
        p = self.parent
        while p.token_type != SYMBOLS.OPERAND:
            p.children.pop()
            p = p.parent
        r_child = self.children[1].reform_graph(parent_vars)
        if r_child.token_type == SYMBOLS.OPER_MULT or r_child.token_type == SYMBOLS.OPER_POW:
            r_child.children.insert(0, self)
            return r_child
        else:
            self.children = [p, r_child]
            return self

        
class SingleParamFuncionNode(CNode):
    def __init__(self, buf=None):
        s = ''.join(buf).lower()
        self.value = s
        if s == 'log':
            super().__init__(SYMBOLS.LOG, s)
        elif s == 'exp':
            super().__init__(SYMBOLS.EXP, s)
        elif s == 'sin':
            super().__init__(SYMBOLS.SIN, s)
        elif s == 'cos':
            super().__init__(SYMBOLS.COS, s)
        elif s == 'tan':
            super().__init__(SYMBOLS.TAN, s)
        elif s == 'csc':
            super().__init__(SYMBOLS.CSC, s)
        elif s == 'sec':
            super().__init__(SYMBOLS.SEC, s)
        elif s == 'cot':
            super().__init__(SYMBOLS.COT, s)
        else:
            super().__init__(SYMBOLS.S_PARAM_FUNC, s)

    def op(self):
        if self.children[0].token_type == SYMBOLS.EXP:
            return math.exp
        elif self.children[0].token_type == SYMBOLS.SIN:
            return math.sin
        elif self.children[0].token_type == SYMBOLS.COS:
            return math.cos
        elif self.children[0].token_type == SYMBOLS.TAN:
            return math.tan
        elif self.children[0].token_type == SYMBOLS.CSC:
            return lambda x: 1.0 / math.sin(x)
        elif self.children[0].token_type == SYMBOLS.SEC:
            return lambda x: 1.0 / math.cos(x)
        elif self.children[0].token_type == SYMBOLS.COT:
            return lambda x: 1.0 / math.tan(x)
        else:
            raise Exception('SingleParamFunc op - Unknown Operator')

    def evaluate(self, var_assign=None):
        return self.value


class CommaNode(CNode):
    def __init__(self, buf=None):
        super().__init__(SYMBOLS.COMMA, buf)


    def evaluate(self, var_assign=None):
        # print(self.children)
        return self.value


class ParenNode(CNode):
    def __init__(self, buf=None):
        if buf == '(':
            super().__init__(SYMBOLS.L_PAREN, buf)
        else:
            super().__init__(SYMBOLS.R_PAREN, buf)


class ExprNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.EXPR, buf)

    def evaluate(self, var_assign=None):
        # EXPR::=OPER_MDP FUNC|TERM|epsilon
        if self.children[0].token_type == SYMBOLS.OPER_MDP:
            return self.children[1].evaluate(var_assign)
        elif self.children[0].token_type == SYMBOLS.TERM:
            return self.children[0].evaluate(var_assign)
        elif self.children[0].token_type == SYMBOLS.epsilon:
            return

    def calculate(self, parent_value, var_assign=None):
        # EXPR::=OPER_MDP FUNC|TERM|epsilon
        if self.children[0].token_type == SYMBOLS.OPER_MDP:
            op = self.children[0].op()
            r_l_value = self.children[1].left(var_assign)
            current_result = op(parent_value, r_l_value)
            result = self.children[1].calculate(current_result, var_assign)
            return result
        elif self.children[0].token_type == SYMBOLS.TERM:
            op = self.children[0].op()

            # r_l_value = self.children[0].left(var_assign)
            # current_result = self.children[0].calculate(r_l_value, var_assign)
            # result = op(parent_value, current_result)

            # l_value = self.children[0].left()
            # t = op(parent_value, l_value)
            # result = self.children[0].calculate(t, var_assign)

            if len(self.children[0].children[1].children[1].children) != 0\
                    and self.children[0].children[1].children[1].children[0].token_type == SYMBOLS.OPER_MDP: # rear operation is */ / /^
                r_l_value = self.children[0].left(var_assign)
                current_result = self.children[0].calculate(r_l_value, var_assign)
                result = op(parent_value, current_result)
            else: # rear operation is + / -
                l_value = self.children[0].left(var_assign)
                t = op(parent_value, l_value)
                result = self.children[0].calculate(t, var_assign)
            return result
        elif self.children[0].token_type == SYMBOLS.epsilon:
            return parent_value

    def op(self):
        return self.children[0].op()

    def left(self, var_assign=None):
        return self.children[0].left(var_assign)

    def reform_graph(self, parent_vars):
        # EXPR::=OPER_MDP FUNC|TERM|epsilon
        if self.children[0].token_type == SYMBOLS.OPER_MDP:
            pass
        elif self.children[0].token_type == SYMBOLS.TERM:
            pass
        elif self.children[0].token_type == SYMBOLS.epsilon:
            return None


class TermNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.TERM, buf)

    def evaluate(self, var_assign=None):
        # TERM::=OPER_PM FUNC
        # called by EXPR when EXPR::=OPER_MDP FUNC
        return self.children[1].evaluate(var_assign)

    def calculate(self, parent_value, var_assign=None):
        # TERM::=OPER_PM FUNC
        # op = self.children[0].op()
        # l_value = self.children[1].left()
        if self.children[1].token_type == SYMBOLS.S_PARAM_FUNC:
            r_value = self.children

        r_value = self.children[1].calculate(parent_value, var_assign)
        result = r_value

        return result

    def op(self):
        return self.children[0].op()

    def left(self, var_assign=None):
        return self.children[1].left(var_assign)


class FactorNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.FACTOR, buf)

    def evaluate(self, var_assign=None):
        # FACTOR::=L_PAREN FUNC R_PAREN
        return self.children[1].evaluate(var_assign)

    def left(self, var_assign):
        return self.children[1].evaluate(var_assign)


class MDPNode(CNode):
    def __init__(self, buf):
        # super().__init__(SYMBOLS.OPER_MDP, buf)
        if buf == '*' or buf == 'OPER_MULT':
            super().__init__(SYMBOLS.OPER_MULT, buf)
            self.op = lambda a, b: a * b
        elif buf == '/' or buf == 'OPER_DIV':
            super().__init__(SYMBOLS.OPER_DIV, buf)
            self.op = lambda a, b: a / b
        elif buf == '^' or buf == 'OPER_POW':
            super().__init__(SYMBOLS.OPER_POW, buf)
            self.op = lambda a, b: a ** b
        elif buf == 'OPER_MDP':
            super().__init__(SYMBOLS.OPER_MDP, buf)
        else:
            raise Exception('MDPNode init - Unknown Operator: {}'.format(buf))

    def op(self):
        if self.children[0].token_type == SYMBOLS.OPER_MULT:
            return lambda a, b: a * b
        elif self.children[0].token_type == SYMBOLS.OPER_DIV:
            return lambda a, b: a / b
        elif self.children[0].token_type == SYMBOLS.OPER_POW:
            return lambda a, b: a ** b
        else:
            raise Exception('MDPNode op - Unknown Operator')


class PMNode(CNode):
    def __init__(self, buf):
        # super().__init__(SYMBOLS.OPER_PM, buf)
        if buf == '+' or buf == 'OPER_PLUS':
            super().__init__(SYMBOLS.OPER_PLUS, buf)
            # self.op = lambda self, a, b: a + b
            # self.t = 'asdf'
            self.op = staticmethod(lambda a, b: a + b)
        elif buf == '-' or buf == 'OPER_MIN':
            super().__init__(SYMBOLS.OPER_MIN, buf)
            self.op = lambda self, a, b: a - b
        elif buf == 'OPER_PM':
            super().__init__(SYMBOLS.OPER_PM, buf)
        else:
            raise Exception('PMNode - Unknown Operator: {}'.format(buf))

    def op(self):
        if self.children[0].token_type == SYMBOLS.OPER_PLUS:
            return lambda a, b: a + b
        elif self.children[0].token_type == SYMBOLS.OPER_MIN:
            return lambda a, b: a - b
        else:
            raise Exception('PMNode op - Unknown Operator')


class StackTopNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.STACK_TOP, buf)


class EpsilonNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.epsilon, buf)

    def evaluate(self, var_assign=None):
        return

    def calculate(self, parent_value, var_assign=None):
        return

    def op(self):
        return None


class LParenNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.L_PAREN, buf)

class RParenNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.R_PAREN, buf)

class ConstNode(CNode):
    def __init__(self, buf=None):
        super().__init__(SYMBOLS.CONST, buf)

    def evaluate(self, var_assign=None):
        return self.children[0].evaluate(var_assign)

class ConstantNode(CNode):
    def __init__(self, token_type, buf=None):
        super().__init__(token_type, buf)
        if buf == 'e':
            self.value = math.e
        elif buf == 'pi':
            self.value = math.pi

    def evaluate(self, var_assign=None):
        return self.value

class NumberNode(CNode):
    def __init__(self, buf=None):
        super().__init__(SYMBOLS.NUMBER, buf)
        if buf is None:
            self.value = 0
        else:
            self.value = float(self.raw_string)

    def evaluate(self, var_assign=None):
        return self.value

    def children_str(self):
        return '[' + self.token_str + ':' + str(self.value) + ']'

class VariableNode(CNode):
    def __init__(self, buf=None):
        super().__init__(SYMBOLS.VARIABLE, buf)
        self.value = ''.join(buf)

    def children_str(self):
        return '[' + self.token_str + ':' + str(self.value) + ']'

    def evaluate(self, var_assign=None):
        if var_assign is None:
            raise Exception('value for {} is not assigned at evaluaion step')
        else:
            if self.value not in var_assign:
                raise Exception('Unassigned Variable: {}'.format(self.value))
            return var_assign[self.value] # ? 이 모양?


class ComputationalGraph(object):
    def __init__(self, root):
        self.root = root
        self.tmp = []

    def __str__(self):
        # return '[' + '[' + str(self.root) + ']' + '-' + self.root.children_str() + ']'
        return self.root.children_str()

    def __repr__(self):
        return self.__str__()

    def reform_graph(self):
        assert(self.root is not None)
        # t = self.root.reform_graph(self.tmp)
        # self.root = t
        self.root.reform_graph(self.tmp)

    def derivative(self, var_name):
        return self.root.derivative(var_name)

    def derivate_all(self):
        exprs = []

        for var in self.variables:
            exprs.append(self.derivate_by(var.name))

        return exprs

    def evaluate(self, var_assign=None):
        return self.root.evaluate(var_assign)

    def is_differentiable(self, var_and_point):
        # sketch
        epsilon = 0.000000000001
        var_and_point = {'x': 10}
        results = {}
        for k, v in var_and_point:
            l_point = v - epsilon
            r_point = v + epsilon
            l_assign = {k: l_point}
            l_limit = self.root.evaluate(l_assign)
            r_assign = {k: r_point}
            r_limit = self.root.evaluate(r_assign)
            if abs(l_limit) - abs(r_limit) < epsilon:
                results[k] = True
            else:
                results[k] = False

    def graph_plot(self, var_name):
        raise NotImplementedError()
