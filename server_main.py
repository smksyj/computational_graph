import tornado.httpserver
import tornado.ioloop
import tornado.web
from computational_graph import ComputationalGraph
from glob import glob
from PIL import Image
import io
import shutil
import json
import ast
import os


IMAGE_PATH = './images'

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('main.html')

    # def post(self):
    #     expr = self.get_argument('expr')
    #     self.application.expr = expr
    #     var_names = self.get_argument('var_names').split(',')
    #     var_assign = {}
    #     for var_val in self.get_argument('var_values').split(','):
    #         k, v = var_val.replace(' ', '').split('=')
    #         var_assign[k] = float(v)
    #     print(expr)
    #     s_point = self.get_argument('s_point')
    #     print('s_point: ', s_point)
    #     d_point = self.get_argument('d_point')
        
    #     if len(expr) != 0:
    #         graph = ComputationalGraph.parse(expr)
    #         self.application.graph = graph
    #         results = []
    #         variables = graph.var_names
    #         result_idxes = []
    #         points = []
    #         values = []
    #         original_evals = []
    #         der_evals = []
    #         canonicalized_forms = []
    #         der_expr = []
    #         dir_der = None

    #         if len(variables) == 2:
    #             # show grad
    #             print('variables: 2')
    #             result_idxes.append(0)

    #             grad = graph.gradient()
    #             # grad = dict((k, grad[k].resolve()) for k in grad)
    #             if len(s_point) != 0:
    #                 s_point = ast.literal_eval(s_point)
    #                 d_point = ast.literal_eval(d_point)
    #                 # print(s_point, d_point)
    #                 dir_der = graph.directional_derivative(s_point, d_point)
    #                 # dir_der = dict((k, dir_der[k].resolve()) for k in dir_der)
    #                 # print('dir_der: ', dir_der)

    #             self.render('result.html', expr=expr,
    #                         two_var=True,
    #                         grad=grad,
    #                         dir_der=dir_der,
    #                         result_idxes=result_idxes,
    #                         variables=variables, results=results)
    #         else:
    #             for idx, variable in enumerate(variables):
    #                 # fig = graph.plot_through(variable, label=expr, save=True)
    #                 try:
    #                     d_graph = graph.derivative(variable)
    #                 except:
    #                     # out of spec
    #                     d_graph = graph.derivative(variable)

    #                 r_graph = d_graph.resolve()

    #                 result_idxes.append(idx)
    #                 points.append(variable)
    #                 values.append(var_assign[variable])
    #                 try:
    #                     original_evals.append(graph.evaluate({variable: var_assign[variable]}))
    #                 except:
    #                     original_evals.append('Not available')
    #                 der_expr.append(d_graph.to_string())
    #                 try:
    #                     der_evals.append(d_graph.evaluate({variable: var_assign[variable]}))
    #                 except:
    #                     der_evals.append('Not available')
    #                 canonicalized_forms.append(r_graph.to_string())
    #                 variables = list(variables)

    #             self.render('result.html', expr=expr,
    #                         two_var=False,
    #                         result_idxes=result_idxes,
    #                         variables=variables, points=points,
    #                         values=values,
    #                         original_evals=original_evals,
    #                         der_expr=der_expr,
    #                         der_evals=der_evals,
    #                         canonicalized_forms=canonicalized_forms)
    #                         # file_name=moved_file_name)


class DerivativeHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('derivative.html')

    def post(self):
        expr = self.get_argument('expr')
        graph = ComputationalGraph.parse(expr)
        var_names = self.get_argument('var_names').split(',')
        derivative_results = {}
        try:
            for var_name in var_names:
                derivative_results[var_name] = graph.derivative(var_name).to_string()
            self.write(json.dumps(derivative_results))
        except NotSupportFunctionException:
            self.send_error(status_code=400)


class EvalHandler(tornado.web.RequestHandler):
    def get(self):
        pass

    def post(self):
        expr = self.get_argument('expr')
        graph = ComputationalGraph.parse(expr)
        var_assign = {}

        for var_val in self.get_argument('var_values').split(','):
            k, v = var_val.replace(' ', '').split('=')
            var_assign[k] = float(v)

        result = graph.evaluate(var_assign)

        self.write(json.dumps(result))


class DirectionalDerivativeHandler(tornado.web.RequestHandler):
    def get(self):
        pass

    def post(self):
        expr = self.get_argument('expr')
        s_point = self.get_argument('s_point')
        d_point = self.get_argument('d_point')
        s_point = ast.literal_eval(s_point)
        d_point = ast.literal_eval(d_point)
        graph = ComputationalGraph.parse(expr)
        try:
            dir_der = graph.directional_derivative(s_point, d_point)
            self.write(str(dir_der))
        except AssertionError:
            self.send_error(status_code=400)
        
    
class GraphHandler(tornado.web.RequestHandler):
    def get(self):
        pass

    def post(self):
        expr = self.get_argument('expr')
        min_range = int(round(float(self.get_argument('min_range'))))
        max_range = int(round(float(self.get_argument('max_range'))))
        n_points = int(round(float(self.get_argument('n_points'))))
        var_name = self.get_argument('var_name')
        graph = ComputationalGraph.parse(expr)
        label = expr
        
        fig = graph.plot(label, var_name=var_name, min_range=min_range, max_range=max_range, n_points=n_points)
        old_file_names = glob(IMAGE_PATH + '/' + '*.jpg')
        for f in old_file_names:
            print('remove {}'.format(f))
            os.remove(f)

        file_name = glob('*.jpg')[0]
        moved_file_name = IMAGE_PATH + '/' + file_name
        print(moved_file_name)
        shutil.move(file_name, moved_file_name)
        self.write(moved_file_name)
       
class CanonicalHandler(tornado.web.RequestHandler):
    def get(self):
        pass

    def post(self):
        expr = self.get_argument('expr')
        graph = ComputationalGraph.parse(expr)
        r_graph = graph.resolve()
        self.write(r_graph.to_string())


settings = {
    'debug': True,
    'autoreload': True,
    # 'static_path': '/home/smksyj/smksyj/computational_graph'
}

application = tornado.web.Application([
    # (r'/', FormulaHandler),
    (r'/', MainHandler),
    (r'/der', DerivativeHandler),
    (r'/eval', EvalHandler),
    (r'/graph', GraphHandler),
    (r'/dir_der', DirectionalDerivativeHandler),
    (r'/cano', CanonicalHandler),
    (r'/images/(.*)', tornado.web.StaticFileHandler, {'path': IMAGE_PATH})
], **settings)

if __name__=='__main__':
    if not os.path.exists(IMAGE_PATH):
        print('making dir for images.')
        os.mkdir(IMAGE_PATH)
    http_server = tornado.httpserver.HTTPServer(application)
    port_number = 8888
    http_server.listen(port_number)
    print('server is waiting...')
    tornado.ioloop.IOLoop.instance().start()
