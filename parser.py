import lexer
import computational_graph
import copy
from pprint import pprint
import cg_utils


class Parser(object):
    def __init__(self, bnf_file_name):
        self.table = self.make_bnf_table(bnf_file_name)
        self.stack = []

    def parse_bnf_file(self, bnf_file_name):
        rules = {}
        terminal_tokens = []
        non_terminal_tokens = []

        for line in open(bnf_file_name, 'r').readlines():
            if line.startswith('#') or len(line) == 0 or line == '\n':
                continue
            elif line.startswith('; terminals'):
                terminal_tokens = line.replace('\n', '').split('=')[1].split(' ')
            else:
                line = line.replace('\n', '')
                s = line.split('::=')
                non_terminal_tokens.append(s[0])
                rules[s[0]] = [(r.split(' ')) for r in s[1].split('|')]
        return terminal_tokens, non_terminal_tokens, rules
        

    def ring_sum(self, one, other):
        if 'epsilon' not in one:
            return one

        t = set(one)
        t.remove('epsilon')
        for tok in other:
            t.add(tok)

        return list(t)


    def calculate_first_from_seq(self, seq, first):
        if 'epsilon' not in first[seq[0]] or len(seq) == 1:
            return first[seq[0]]
        else:
            t = copy.deepcopy(first[seq[0]])
            t.remove('epsilon')
            try:
                res = t.union(self.calculate_first_from_seq(seq[1], first))
            except:
                print(first)
            return res


    def make_first(self, terminal_tokens, non_terminal_tokens, rules):
        first = {}
        c_rules = copy.deepcopy(rules)
        remove_idx = []

        for term in terminal_tokens:
            first[term] = set()
            first[term].add(term)
        for non_term in non_terminal_tokens:
            # print(non_term)
            first[non_term] = set()
            derivated_rules = c_rules[non_term]
            for d_rule_idx in range(len(derivated_rules)):
                first_tok = derivated_rules[d_rule_idx][0]
                if first_tok in terminal_tokens:
                    first[non_term] = first[non_term].union(first[first_tok])
                    # print(non_term, first[non_term], first[first_tok], derivated_rules)
                    remove_idx.append(d_rule_idx)
                    continue
            for idx in reversed(remove_idx):
                del derivated_rules[idx]
            # print(non_term, derivated_rules, c_rules)
            remove_idx.clear()

        before_first = copy.deepcopy(first)
        while True:
            for non_term in non_terminal_tokens:
                for d_rule in c_rules[non_term]:
                    # print(non_term, d_rule, first[non_term])
                    t = first[d_rule[0]]
                    # print(t)
                    for tok in d_rule[1:]:
                        t = self.ring_sum(t, first[tok])
                        # print(t)
                    first[non_term] = first[non_term].union(t)
            if before_first == first:
                break
            else:
                before_first = copy.deepcopy(first)

        # pprint(first)
        return first


    def make_follow(self, terminal_tokens, non_terminal_tokens, rules, first):
        follow = {}
        follow['EXPR'] = set()
        follow['EXPR'].add('STACK_TOP')

        # print(non_terminal_tokens)
        for non_term in non_terminal_tokens:
            d_rules = rules[non_term]
            for d_rule in d_rules:
                follow_idx = reversed(range(len(d_rule)-1))
                last_idx = len(d_rule) - 1

                if d_rule[last_idx] in non_terminal_tokens:
                    if d_rule[last_idx] not in follow:
                        follow[d_rule[last_idx]] = set()
                    if non_term not in follow:
                        follow[non_term] = set()
                    follow[d_rule[last_idx]] = follow[d_rule[last_idx]].union(follow[non_term])
                for idx in follow_idx:
                    # non_term -> alpha d_rule[idx] beta & beta => eos
                    if d_rule[idx] not in follow:
                        follow[d_rule[idx]] = set()
                    if 'epsilon' in first[d_rule[idx]]:
                        follow[d_rule[idx]] = follow[d_rule[idx]].union(follow[non_term])
                        continue
                    t = copy.deepcopy(first[d_rule[idx+1]])
                    if 'epsilon' in t:
                        follow[d_rule[idx]] = follow[d_rule[idx]].union(follow[non_term])
                    follow[d_rule[idx]] = follow[d_rule[idx]].union(t)

        for non_term in non_terminal_tokens:
            d_rules = rules[non_term]
            for d_rule in d_rules:
                follow_idx = reversed(range(len(d_rule) - 1))
                last_idx = len(d_rule) - 1

                if d_rule[last_idx] in non_terminal_tokens:
                    if d_rule[last_idx] not in follow:
                        follow[d_rule[last_idx]] = set()
                    if non_term not in follow:
                        follow[non_term] = set()
                    follow[d_rule[last_idx]] = follow[d_rule[last_idx]].union(follow[non_term])
                for idx in follow_idx:
                    # non_term -> alpha d_rule[idx] beta & beta => eos
                    if d_rule[idx] not in follow:
                        follow[d_rule[idx]] = set()
                    if 'epsilon' in first[d_rule[idx]]:
                        follow[d_rule[idx]] = follow[d_rule[idx]].union(follow[non_term])
                        continue
                    t = copy.deepcopy(first[d_rule[idx + 1]])
                    if 'epsilon' in t:
                        follow[d_rule[idx]] = follow[d_rule[idx]].union(follow[non_term])
                    follow[d_rule[idx]] = follow[d_rule[idx]].union(t)
        # pprint(follow)
        return follow


    def make_bnf_table(self, bnf_file_name):
        terminal_tokens, non_terminal_tokens, rules = self.parse_bnf_file(bnf_file_name)
        table = {}

        self.first = self.make_first(terminal_tokens, non_terminal_tokens, rules)
        self.follow = self.make_follow(terminal_tokens, non_terminal_tokens, rules, self.first)

        # pprint(self.first)
        # pprint(self.follow)
        
        for non_term in rules.keys():
            for d_rule in rules[non_term]:
                # print(non_term, d_rule)
                first_symbols = self.calculate_first_from_seq(d_rule, self.first)

                if 'epsilon' in first_symbols:
                    # print(non_term, self.follow[non_term])
                    for follow_tok in self.follow[non_term]:
                        table[(non_term, follow_tok)] = d_rule
                else:
                    for first_tok in first_symbols:
                        table[(non_term, first_tok)] = d_rule

        # pprint(table)
        return table


    @staticmethod
    def make_node_from(token_str):
        if token_str == 'OPERAND':
            return computational_graph.OperandNode(token_str)
        elif token_str == 'CONST':
            return computational_graph.ConstNode(token_str)
        elif token_str == 'S_PARAM_FUNC' or token_str == 'LOG' or token_str == 'EXP' or \
                token_str == 'SIN' or token_str == 'COS' or token_str == 'TAN' or \
                token_str == 'CSC' or token_str == 'SEC' or token_str == 'COT' or token_str == 'POW':
            return computational_graph.SingleParamFuncionNode(token_str)
        elif token_str == 'EXPR':
            return computational_graph.ExprNode(token_str)
        elif token_str == 'TERM':
            return computational_graph.TermNode(token_str)
        elif token_str == 'FACTOR':
            return computational_graph.FactorNode(token_str)
        elif token_str == 'OPER_PLUS':
            return computational_graph.PlusNode(token_str)
        elif token_str == 'OPER_MIN':
            return computational_graph.MinusNode(token_str)
        elif token_str == 'OPER_MULT':
            return computational_graph.MultNode(token_str)
        elif token_str == 'OPER_DIV':
            return computational_graph.DivNode(token_str)
        elif token_str == 'NUMBER':
            return computational_graph.NumberNode()
        elif token_str == 'VARIABLE':
            return computational_graph.VariableNode(token_str)
        elif token_str == 'epsilon':
            return computational_graph.EpsilonNode(token_str)
        elif token_str == 'L_PAREN':
            return computational_graph.LParenNode(token_str)
        elif token_str == 'R_PAREN':
            return computational_graph.RParenNode(token_str)
        elif token_str == 'COMMA':
            return computational_graph.CommaNode(token_str)
        elif token_str == 'EXPRTAIL':
            return computational_graph.ExprTailNode(token_str)
        elif token_str == 'TERMTAIL':
            return computational_graph.TermTailNode(token_str)
        elif token_str == 'E':
            return computational_graph.ENode(token_str)
        elif token_str == 'PI':
            return computational_graph.PINode(token_str)
        else:
            raise Exception('{}: make_node_from exception'.format(token_str))


    @staticmethod
    def make_nodes_from(rule):
        nodes = []
        for non_term in rule:
            nodes.append(Parser.make_node_from(non_term))
        return nodes


    def parse(self, in_tokens, debug=False):
        current = computational_graph.ExprNode('EXPR')
        root = current
        self.stack.append(current)
        self.stack.append(computational_graph.StackTopNode('$'))
        in_tokens.append(computational_graph.StackTopNode('$'))
        var_names = set()

        # pprint(self.table)
        if debug:
            print('in_tokens: ', in_tokens)

        while len(in_tokens) > 0:
            # print(self.stack[0].token_str)
            if self.stack[0].token_str == 'epsilon':
                self.stack.pop(0)
                continue

            if self.stack[0].token_str == in_tokens[0].token_str:
                # pop
                top_token = self.stack.pop(0) # terminal token string
                in_token = in_tokens.pop(0) # terminal node with value
                if top_token.token_str == 'STACK_TOP':
                    continue

                if top_token.token_str == 'CONST' or top_token.token_str == 'VARIABLE' or top_token.token_str == 'NUMBER':
                    current.children.pop(0)
                    current.children.insert(0, in_token)
                    if top_token.token_str == 'VARIABLE':
                        var_names.add(in_token.value)

                if debug:
                    print('matched term: {}, {}'.format(top_token, in_token.token_str))
                    print('top_token: ', top_token)
                    print('stack: ', self.stack)
                    print()
            else:
                # extend (POP non-term and Add new rule)
                top_token = self.stack.pop(0) # non-terminal token string
                rule = self.table[top_token.token_str, in_tokens[0].token_str]

                if debug:
                    print('top_token: ', top_token)
                    print('rule: ', rule)
                token_nodes = Parser.make_nodes_from(rule)
                self.stack = token_nodes + self.stack
                if debug:
                    print('current stack: ', self.stack)
                    print()
                current = top_token
                current.children.extend(token_nodes)

        if len(self.stack) != 0:
            raise Exception('parsing error')
        else:
            graph = computational_graph.ComputationalGraph(root, var_names)
            return graph


if __name__=='__main__':
    lex = lexer.Lexer()
    bnf_file_name = 'bnf.txt'
    pars = Parser(bnf_file_name)
    lines = map(lambda line: line.replace('\n', ''), open('input.txt', 'r').readlines())
    debug = False

    for line in lines:
        if len(line) == 0 or line == '\n' or line.startswith('#'):
            continue
        line, var_assign, der_names = cg_utils.split_line(line)
        # print(line, var_assign)
        tokens = lex.tokenize(line)
        # print(tokens)
        graph = pars.parse(tokens, debug)
        print('{}: {}'.format(line, graph))
        eval_result = graph.evaluate(var_assign)
        resolved_graph = graph.resolve()
        # print(eval_result)
