import copy
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import lexer
import parser
import datetime
from PIL import Image
from terminal_nodes import *
import os


class BinaryOperatorNode(CNode):
    def __init__(self, token_type, buf, lower_type, tailnode_type):
        super().__init__(token_type, buf)
        self.lower_type = lower_type
        self.tailnode_type = tailnode_type

    def is_last_node(self):
        return self.children[1].is_epsilon()

    def is_zero(self):
        return self.children[0].is_zero() and self.is_last_node()

    def is_one(self):
        return self.children[0].is_one() and self.is_last_node()

    def evaluate(self, var_assign=None):
        if self.is_last_node():
            # EXPR::=TERM
            result = self.children[0].evaluate(var_assign)
            return result
        else:
            # EXPR::=TERM EXPRTAIL
            l_value = self.children[0].evaluate(var_assign)
            result = self.children[1].calculate(l_value, var_assign)
            return result

    def calculate(self, parent_value, var_assign=None):
        if self.is_last_node():
            # This parent_value is calculated from higher layer
            return parent_value
        else:
            op = self.children[1].op()
            r_l_value = self.children[1].children[1].evaluate(var_assign)
            result = op(parent_value, r_l_value)
            return result

    def resolve(self, parent_func=None):
        if parent_func is None:
            left_func = self.children[0].resolve()
        else:
            left_func = parent_func

        if self.is_last_node():
            if not left_func.has_variable():
                left_val = left_func.evaluate()
                # left_val = left_func.resolve()
                left_func = self.lower_type.from_number(left_val)

        resolved_node = self.children[1].resolve(left_func)
        return resolved_node

    def has_variable(self):
        if self.is_last_node():
            return self.children[0].has_variable()
        else:
            return self.children[0].has_variable() or self.children[1].has_variable()

    def has_variable_name(self, var_name):
        if self.is_last_node():
            # TERM::=FACTOR
            return self.children[0].has_variable_name(var_name)
        else:
            # TERM::=FACTOR TERMTAIL
            return self.children[0].has_variable_name(var_name) or self.children[1].has_variable_name(var_name)

    def to_string(self):
        l_str = self.children[0].to_string()
        return self.children[1].to_string(l_str)


class TailNode(CNode):
    def __init__(self, token_type, buf):
        super().__init__(token_type, buf)

    def is_epsilon(self):
        return self.children[0].token_type == SYMBOLS.epsilon

    def op(self):
        return self.children[0].op()

    def op_name(self):
        return self.children[0].raw_string

    def has_variable(self):
        return not self.is_epsilon() and self.children[1].has_variable()

    def has_variable_name(self, var_name):
        return not self.is_epsilon() and self.children[1].has_variable_name(var_name)

    def calculate(self, parent_value, var_assign=None):
        # EXPRTAIL::=OPER_PLUS EXPR|OPER_MIN EXPR
        if self.children[1].is_last_node():
            # EXPRTAIL::=OPER_PLUS EXPR OPER_PLUS EXPR ...
            # reduce from parent_value and left_value of child EXPR
            op = self.children[0].op()
            r_l_value = self.children[1].evaluate(var_assign)
            result = op(parent_value, r_l_value)
            return result
        else:
            op = self.children[0].op()
            _expr = self.children[1]
            # get evaluate result EXPR -> TERM
            r_l_value = _expr.children[0].evaluate(var_assign)
            current_result = op(parent_value, r_l_value)
            result = self.children[1].calculate(current_result, var_assign)
            return result

    def to_string(self, p_str):
        if self.is_epsilon():
            return p_str
        return p_str + self.children[0].to_string() + self.children[1].to_string()


class ExprNode(BinaryOperatorNode):
    def __init__(self, buf='EXPR'):
        super().__init__(SYMBOLS.EXPR, buf, TermNode, ExprTailNode)

    def derivative(self, variable_name):
        # get derivative and wrap with proper node
        if self.is_last_node():
            # EXPR::=TERM EXPRTAIL and EXPRTAIL is epsilon
            l_der = self.children[0].derivative(variable_name)
            result = ExprNode.from_term(l_der)
            return result
        else:
            # EXPR::=TERM EXPRTAIL
            l_der = self.children[0].derivative(variable_name) # TERM
            op_name = self.children[1].op_name()
            r_der = self.children[1].children[1].derivative(variable_name) # EXPR

            if l_der.is_zero():
                return r_der
            elif r_der.is_zero():
                return ExprNode.from_term(l_der)
            else:
                return ExprNode.from_term(l_der, op_name, r_der)

    @classmethod
    def from_term(cls, term, op_name=None, expr=None):
        assert term.token_type == SYMBOLS.TERM
        node = ExprNode('EXPR')
        node.children.append(term)
        exprtail = ExprTailNode.from_elements(op_name, expr)
        node.children.append(exprtail)
        return node

    @classmethod
    def from_factor(cls, factor):
        assert factor.token_type == SYMBOLS.FACTOR
        term = TermNode.from_factor(factor)
        return ExprNode.from_term(term)

    @classmethod
    def from_expr(cls, expr):
        assert expr.token_type == SYMBOLS.EXPR
        factor = FactorNode.from_expr(expr)
        return ExprNode.from_factor(factor)

    @classmethod
    def from_number(cls, num):
        term = TermNode.from_number(num)
        exprtail = ExprTailNode.from_elements()
        expr = ExprNode('EXPR')
        expr.children.append(term)
        expr.children.append(exprtail)
        return expr

    @classmethod
    def from_variable(cls, var_name):
        return ExprNode.from_term(TermNode.from_variable(var_name))

    def multiply(self, value):
        # TERM::=FACTOR * TERM
        # print('multiply value: ', value)
        try:
            value = float(value)
            factor_node = FactorNode.from_expr(self)
            term_node = TermNode.from_number(value)
        except ValueError:
            factor_node = FactorNode.from_expr(self)
            term_node = TermNode.from_variable(value)
        finally:
            new_term_node = TermNode.from_factor(factor_node, 'OPER_MULT', term_node)
            new_expr_node = ExprNode.from_term(new_term_node)

            # print(new_expr_node.to_string())
            return new_expr_node


class ExprTailNode(TailNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.EXPRTAIL, buf)

    def resolve(self, parent_term):
        if self.is_epsilon():
            new_expr_node = ExprNode.from_term(parent_term)
            return new_expr_node

        term = self.children[1].children[0].resolve()
        r_expr = self.children[1].resolve()

        if parent_term.is_zero():
            if self.op_name() == 'OPER_MIN':
                parent_term = TermNode.from_factor(FactorNode.make_min_factor(ExprNode.from_term(term)).resolve())
            else:
                parent_term = term.resolve()
            new_expr_node = r_expr.resolve(parent_term)
            return new_expr_node
        if r_expr.is_zero():
            new_expr_node = ExprNode.from_term(parent_term)
            return new_expr_node

        p_has_var = parent_term.has_variable()
        f_has_var = term.has_variable()

        if not p_has_var and not f_has_var: # n op n ...
            parent_val = parent_term.evaluate()
            term_val = term.evaluate()
            op = self.children[0].op()
            val = op(parent_val, term_val)
            val = TermNode.from_number(val)
            new_expr = r_expr.resolve(val)
            return new_expr
        elif p_has_var: # x op x ... | x op n
            op_name = self.op_name()
            new_expr = r_expr.resolve()

            if new_expr.is_zero() and (op_name == 'OPER_PLUS' or op_name == 'OPER_MIN'): # x +/- 0
                return ExprNode.from_term(parent_term)
            else:
                result = ExprNode.from_term(parent_term, op_name, new_expr)
            return result
        else: # n op x ...
            op_name = self.op_name()
            new_expr = r_expr.resolve(parent_term)
            new_term = TermNode.from_expr(new_expr)
            new_expr2 = ExprNode.from_term(term)
            result = ExprNode.from_term(new_term, op_name, new_expr2)
            return result

    @classmethod
    def from_elements(cls, op_name=None, expr=None):
        exprtail = ExprTailNode('EXPRTAIL')
        if expr is None:
            exprtail.children.append(EpsilonNode('epsilon'))
        else:
            if op_name == 'OPER_PLUS':
                op_node = PlusNode('OPER_PLUS')
            elif op_name == 'OPER_MIN':
                op_node = MinusNode('OPER_MIN')
            else:
                raise Exception('wrong operation')
            exprtail.children.append(op_node)
            exprtail.children.append(expr)
        return exprtail


class TermNode(BinaryOperatorNode):
    def __init__(self, buf='TERM'):
        super().__init__(SYMBOLS.TERM, buf, FactorNode, TermTailNode)

    def derivative(self, variable_name):
        if self.is_last_node():
            # TERM::=FACTOR
            l_der = self.children[0].derivative(variable_name)
            new_term_node = TermNode.from_factor(l_der)
            return new_term_node
        else:
            # TERM::=FACTOR TERMTAIL
            l_func = self.children[0] # factor
            r_func = self.children[1].children[1] # term
            op_name = self.children[1].op_name()
            l_der = self.children[0].derivative(variable_name) # factor
            r_der = self.children[1].children[1].derivative(variable_name) # term

            if op_name == 'OPER_MULT':
                return TermNode.multiplication_derivative(l_func, r_func, l_der, r_der)
            elif op_name == 'OPER_DIV':
                return TermNode.division_derivative(l_func, r_func, l_der, r_der)
            else:
                raise Exception('unexpected operator')

    @classmethod
    def from_factor(cls, factor, op_name=None, term=None):
        node = TermNode('TERM')
        node.children.append(factor)
        termtail = TermTailNode.from_elements(op_name, term)
        node.children.append(termtail)
        return node

    @classmethod
    def from_expr(cls, expr):
        assert expr.token_type == SYMBOLS.EXPR
        factor = FactorNode.from_expr(expr)
        return TermNode.from_factor(factor)

    @classmethod
    def from_term(cls, term):
        assert term.token_type == SYMBOLS.TERM
        expr = ExprNode.from_term(term)
        return TermNode.from_expr(expr)

    @classmethod
    def from_variable(cls, var_name):
        return TermNode.from_factor(FactorNode.from_variable(var_name))

    @classmethod
    def multiplication_derivative(cls, l_func, r_func, l_der, r_der):
        # (fg)' = f'g + fg' -> return node is ExprNode
        if l_der.is_zero() and r_der.is_zero():
            result = TermNode.from_factor(l_der)
        elif l_der.is_zero():
            # return f(x) * g'(x)
            result = TermNode.from_factor(l_func, 'OPER_MULT', r_der)
        elif r_der.is_zero():
            # return f'(x) * g(x)
            result = TermNode.from_factor(l_der, 'OPER_MULT', r_func)
        else:
            l_val = TermNode.from_factor(l_der, 'OPER_MULT', r_func) # TERM
            r_val = TermNode.from_factor(l_func, 'OPER_MULT', r_der) # TERM
            r_val = ExprNode.from_term(r_val) # EXPR
            result = ExprNode.from_term(l_val, 'OPER_PLUS', r_val) # EXPR::=TERM EXPRTAIL

            result = FactorNode.from_expr(result)
            result = TermNode.from_factor(result)
        return result

    @classmethod
    def division_derivative(cls, l_func, r_func, l_der, r_der):
        # r_func: TERM
        # (f(x)/g(x))' = ( f'(x)g(x) - f(x)g'(x) ) / ( g(x) ) ^ 2
        if l_der.is_zero() and r_der.is_zero():
            result = TermNode.from_factor(l_der)
            return result
        elif l_der.is_zero():
            r_val = TermNode.from_factor(l_func, 'OPER_MULT', r_der)
            r_val = ExprNode.from_term(r_val)
            numerator = FactorNode.make_min_factor(r_val)
        elif r_der.is_zero():
            l_val = TermNode.from_factor(l_der, 'OPER_MULT', r_func)
            l_val = ExprNode.from_term(l_val) # added
            numerator = FactorNode.from_expr(l_val)
        else:
            l_val = TermNode.from_factor(l_der, 'OPER_MULT', r_func)
            r_val = TermNode.from_factor(l_func, 'OPER_MULT', r_der)
            r_val = ExprNode.from_term(r_val)
            numerator = ExprNode.from_term(l_val, 'OPER_MIN', r_val)
            numerator = FactorNode.from_expr(numerator)

        r_func = ExprNode.from_term(r_func)
        denominator = FactorNode.make_pow(r_func, ExprNode.from_number('2'))
        denominator = TermNode.from_factor(denominator)

        result = TermNode.from_factor(numerator, 'OPER_DIV', denominator)
        return result

    @classmethod
    def from_number(cls, num):
        factor_node = FactorNode.from_number(num)
        term_node = TermNode('TERM')
        term_tail_node = TermTailNode.from_elements()
        term_node.children.append(factor_node)
        term_node.children.append(term_tail_node)
        return term_node


class TermTailNode(TailNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.TERMTAIL, buf)

    def resolve(self, parent_factor=None):
        if self.is_epsilon():
            new_term_node = TermNode.from_factor(parent_factor)
            return new_term_node

        factor = self.children[1].children[0].resolve()
        r_term = self.children[1].resolve()

        if parent_factor.is_zero(): # 0 op ...
            return TermNode.from_number('0')
        if parent_factor.is_one() and self.op_name() == 'OPER_MULT': # 1 * ...
            return self.children[1].resolve()

        p_has_var = parent_factor.has_variable()
        f_has_var = factor.has_variable()

        if not p_has_var and not f_has_var: # n op n ...
            parent_val = parent_factor.evaluate()
            factor_val = factor.evaluate()
            op = self.children[0].op()
            val = op(parent_val, factor_val)
            val = FactorNode.from_number(val)
            new_term_node = r_term.resolve(val)
            return new_term_node
        elif p_has_var: # x op x ... | x op n
            op_name = self.op_name()
            new_term_node = r_term.resolve()

            if new_term_node.is_zero() and op_name == 'OPER_MULT': # x * 0
                return TermNode.from_number('0')
            if new_term_node.is_one() and op_name == 'OPER_MULT': # x * 1
                result = TermNode.from_factor(parent_factor)
            else:
                result = TermNode.from_factor(parent_factor, op_name, new_term_node)

            return result
        else: # n op x ...
            op_name = self.op_name()
            new_term = r_term.resolve(parent_factor) # self.children[1] == TERM
            new_factor = FactorNode.from_term(new_term)
            new_term2 = TermNode.from_factor(factor)
            result = TermNode.from_factor(new_factor, op_name, new_term2)
            return result

    @classmethod
    def from_elements(cls, op_name=None, term=None):
        termtail = TermTailNode('TERMTAIL')
        if term is None:
            termtail.children.append(EpsilonNode('epsilon'))
        else:
            if op_name == 'OPER_MULT':
                op_node = MultNode('OPER_MULT')
            elif op_name == 'OPER_DIV':
                op_node = DivNode('OPER_DIV')
            else:
                raise Exception('wrong operator type in from_elements')
            termtail.children.append(op_node)
            termtail.children.append(term)
        return termtail


class FactorNode(CNode):
    def __init__(self, buf):
        super().__init__(SYMBOLS.FACTOR, buf)

    def evaluate(self, var_assign=None):
        if self.children[0].token_type == SYMBOLS.L_PAREN:
            # FACTOR::=L_PAREN EXPR R_PAREN
            return self.children[1].evaluate(var_assign)
        elif self.children[0].token_type == SYMBOLS.OPERAND:
            return self.children[0].evaluate(var_assign)
        elif self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:
            # FACTOR::=S_PARAM_FUNC L_PAREN EXPR R_PAREN
            op = self.children[0].op()
            result = op(self.children[2].evaluate(var_assign))
            return result
        elif self.children[0].token_type == SYMBOLS.LOG:
            # FACTOR::=LOG L_PAREN EXPR COMMA EXPR R_PAREN
            base = self.children[2].evaluate(var_assign)
            x = self.children[4].evaluate(var_assign)
            return math.log(x, base)
        elif self.children[0].token_type == SYMBOLS.OPER_MIN:
            # FACTOR::=OPER_MIN OPERAND
            return -self.children[1].evaluate(var_assign)
        elif self.children[0].token_type == SYMBOLS.POW:
            # FACTOR::=POW ( EXPR , EXPR )
            base = self.children[2].evaluate(var_assign)
            val = self.children[4].evaluate(var_assign)
            result = math.pow(base, val)
            return result

    def derivative(self, variable_name):
        if self.children[0].token_type == SYMBOLS.L_PAREN:
            # FACTOR::=L_PAREN EXPR R_PAREN
            result = self.children[1].derivative(variable_name)
            return FactorNode.from_expr(result)
        elif self.children[0].token_type == SYMBOLS.OPERAND:
            l_der = self.children[0].derivative(variable_name)
            result = FactorNode.from_operand(l_der)
            return result
        elif self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:
            # FACTOR::=S_PARAM_FUNC L_PAREN EXPR R_PAREN
            if not self.children[2].has_variable_name(variable_name):
                return FactorNode.from_number('0')

            in_func = self.children[2] # EXPR
            func_name = self.children[0].children[0].token_str
            out_der = FactorNode.single_param_func_derivative(func_name, in_func)
            in_der = in_func.derivative(variable_name) # EXPR
            in_der = TermNode.from_expr(in_der)
            result = TermNode.from_factor(out_der, 'OPER_MULT', in_der)
            result = FactorNode.from_term(result)
            return result
        elif self.children[0].token_type == SYMBOLS.LOG:
            if self.children[2].has_variable():
                # log(f(x), ...)
                raise Exception('Not supported type of function')
            if not self.children[4].has_variable_name(variable_name):
                return FactorNode.from_number('0')

            # FACTOR::=LOG L_PAREN EXPR COMMA EXPR R_PAREN
            # log(a, f(x)) -> (1 / (x * ln(a))) * f(x)'
            base_expr = self.children[2]
            value_expr = self.children[4]

            value_factor = FactorNode.from_expr(value_expr)
            value_der_expr = value_expr.derivative(variable_name)

            c = ExprNode.from_number('E')
            lna = FactorNode.make_log(c, base_expr) # ln(a)
            lna_term = TermNode.from_factor(lna)
            denominator = TermNode.from_factor(value_factor, 'OPER_MULT', lna_term) # x * ln(a)
            denominator = TermNode.from_term(denominator)

            one = OperandNode.from_number('1')
            one = FactorNode.from_operand(one)
            log_der = TermNode.from_factor(one, 'OPER_DIV', denominator) # 1 / (x * ln(a))

            # for composite function
            value_der_term = TermNode.from_expr(value_der_expr)
            result = TermNode.from_factor(log_der, 'OPER_MULT', value_der_term) # (1 / (x * ln(a))) * f(x)'
            result = FactorNode.from_term(result)

            return result
        elif self.children[0].token_type == SYMBOLS.OPER_MIN:
            # FACTOR::=OPER_MIN OPERAND
            factor_node = self.children[1].derivative(variable_name)
            expr_node = ExprNode.from_factor(factor_node)
            return FactorNode.make_min_factor(expr_node)
        elif self.children[0].token_type == SYMBOLS.POW:
            # FACTOR::=POW ( EXPR , EXPR )
            if self.children[2].has_variable() and not self.children[4].has_variable():
                if not self.children[2].has_variable_name(variable_name):
                    return FactorNode.from_number('0')

                # f(x) ^ n -> n * f(x) ^ (n-1) * f(x)'
                base = self.children[2] # EXPR
                val = self.children[4] # EXPR
                val = TermNode.from_expr(val)

                degree = ExprNode.from_term(val, 'OPER_MIN', ExprNode.from_number(1)) # (n-1), EXPR
                result = FactorNode.make_pow(base, degree) # f(x) ^ (n-1)
                result = TermNode.from_factor(result, 'OPER_MULT', val) # (f(x) ^ (n-1)) * n
                base_der = base.derivative(variable_name)  # f(x)', EXPR
                base_der_factor = FactorNode.from_expr(base_der)
                result = TermNode.from_factor(base_der_factor, 'OPER_MULT', result) # f(x)' * (f(x) ^ (n-1)) * n
                result = FactorNode.from_expr(ExprNode.from_term(result))
                return result
            elif not self.children[2].has_variable() and self.children[4].has_variable():
                if not self.children[4].has_variable_name(variable_name):
                    return FactorNode.from_number('0')

                # a^f(x) -> a^f(x) * ln(a) * f(x)'
                base = self.children[2] # EXPR
                val = self.children[4] # EXPR
                r_val = FactorNode.make_log(ExprNode.from_number('E'), base) # ln(a)
                r_val = TermNode.from_factor(r_val)

                result = TermNode.from_factor(self, 'OPER_MULT', r_val)  # a^f(x) * ln(a)
                val_der = val.derivative(variable_name) # f(x)', EXPR
                val_der_factor = FactorNode.from_expr(val_der)
                result = TermNode.from_factor(val_der_factor, 'OPER_MULT', result) # a^f(x) * ln(a) * f(x)'
                result = FactorNode.from_expr(ExprNode.from_term(result))
                return result
            else:
                # f(x) ^ f(x) <- out of spec
                raise NotSupportFunctionException('Not supported function type')

    @classmethod
    def from_operand(cls, val):
        factor_node = FactorNode('FACTOR')
        factor_node.children.append(val)
        return factor_node

    @classmethod
    def from_number(cls, num):
        operand_node = OperandNode.from_number(num)
        factor_node = FactorNode('FACTOR')
        factor_node.children.append(operand_node)
        return factor_node

    @classmethod
    def from_expr(cls, expr):
        assert expr.token_type == SYMBOLS.EXPR
        factor_node = FactorNode('FACTOR')
        factor_node.children.append(LParenNode('L_PAREN'))
        factor_node.children.append(expr)
        factor_node.children.append(RParenNode('R_PAREN'))
        return factor_node

    @classmethod
    def from_term(cls, term):
        assert term.token_type == SYMBOLS.TERM
        expr = ExprNode.from_term(term)
        return FactorNode.from_expr(expr)

    @classmethod
    def make_log(cls, base_expr, value_expr):
        assert base_expr.token_type == SYMBOLS.EXPR
        assert value_expr.token_type == SYMBOLS.EXPR
        factor_node = FactorNode('FACTOR')
        factor_node.children.append(SingleParamFuncionNode('LOG'))
        factor_node.children.append(LParenNode('L_PAREN'))
        factor_node.children.append(base_expr)
        factor_node.children.append(CommaNode('COMMA'))
        factor_node.children.append(value_expr)
        factor_node.children.append(RParenNode('R_PAREN'))
        return factor_node

    @classmethod
    def make_pow(cls, base_expr, value_expr):
        assert base_expr.token_type == SYMBOLS.EXPR
        assert value_expr.token_type == SYMBOLS.EXPR
        factor_node = FactorNode('FACTOR')
        factor_node.children.append(SingleParamFuncionNode('POW'))
        factor_node.children.append(LParenNode('L_PAREN'))
        factor_node.children.append(base_expr)
        factor_node.children.append(CommaNode('COMMA'))
        factor_node.children.append(value_expr)
        factor_node.children.append(RParenNode('R_PAREN'))
        return factor_node

    @classmethod
    def make_s_param_func(cls, func_name, in_func_expr):
        assert in_func_expr.token_type == SYMBOLS.EXPR
        factor_node = FactorNode('FACTOR')
        s_func = SingleParamFuncionNode('S_PARAM_FUNC')
        s_func.children.append(SingleParamFuncionNode(func_name))
        factor_node.children.append(s_func)
        factor_node.children.append(LParenNode('L_PAREN'))
        factor_node.children.append(in_func_expr)
        factor_node.children.append(RParenNode('R_PAREN'))
        return factor_node

    @classmethod
    def make_min_factor(cls, expr):
        assert expr.token_type == SYMBOLS.EXPR
        factor_node = FactorNode('FACTOR')
        factor_node.children.append(MinusNode('OPER_MIN'))
        factor_node.children.append(FactorNode.from_expr(expr))
        return factor_node

    @classmethod
    def from_variable(cls, var_name):
        factor_node = FactorNode('FACTOR')
        factor_node.children.append(OperandNode.from_variable(var_name))
        return factor_node

    def is_zero(self):
        if self.children[0].token_type == SYMBOLS.L_PAREN:
            # FACTOR::=L_PAREN EXPR R_PAREN
            return self.children[1].is_zero()
        elif self.children[0].token_type == SYMBOLS.OPERAND:
            return self.children[0].is_zero()
        elif self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:
            # FACTOR::=S_PARAM_FUNC L_PAREN EXPR R_PAREN
            return self.children[2].is_zero()
        elif self.children[0].token_type == SYMBOLS.LOG:
            # FACTOR::=LOG L_PAREN EXPR COMMA EXPR R_PAREN
            return self.children[4].is_zero()
        elif self.children[0].token_type == SYMBOLS.OPER_MIN:
            # FACTOR::=OPER_MIN OPERAND
            return self.children[1].is_zero()

    def is_one(self):
        return (self.children[0].token_type == SYMBOLS.OPERAND and self.children[0].is_one()) or \
               (self.children[0].token_type == SYMBOLS.L_PAREN and self.children[1].is_one())

    # TODO: change to store has variable
    def has_variable(self):
        if self.children[0].token_type == SYMBOLS.L_PAREN:
            # FACTOR::=L_PAREN EXPR R_PAREN
            return self.children[1].has_variable()
        elif self.children[0].token_type == SYMBOLS.OPERAND:
            return self.children[0].has_variable()
        elif self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:
            # FACTOR::=S_PARAM_FUNC L_PAREN EXPR R_PAREN
            return self.children[2].has_variable()
        elif self.children[0].token_type == SYMBOLS.LOG:
            # FACTOR::=LOG L_PAREN EXPR COMMA EXPR R_PAREN
            return self.children[2].has_variable() or self.children[4].has_variable()
        elif self.children[0].token_type == SYMBOLS.OPER_MIN:
            # FACTOR::=OPER_MIN OPERAND
            return self.children[1].has_variable()
        elif self.children[0].token_type == SYMBOLS.POW:
            # FACTOR::POW ( EXPR , EXPR )
            return self.children[2].has_variable() or self.children[4].has_variable()

    def has_variable_name(self, var_name):
        if self.children[0].token_type == SYMBOLS.L_PAREN:
            # FACTOR::=L_PAREN EXPR R_PAREN
            return self.children[1].has_variable_name(var_name)
        elif self.children[0].token_type == SYMBOLS.OPERAND:
            return self.children[0].has_variable_name(var_name)
        elif self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:
            # FACTOR::=S_PARAM_FUNC L_PAREN EXPR R_PAREN
            return self.children[2].has_variable_name(var_name)
        elif self.children[0].token_type == SYMBOLS.LOG:
            # FACTOR::=LOG L_PAREN EXPR COMMA EXPR R_PAREN
            return self.children[2].has_variable_name(var_name) or self.children[4].has_variable_name(var_name)
        elif self.children[0].token_type == SYMBOLS.OPER_MIN:
            # FACTOR::=OPER_MIN OPERAND
            return self.children[1].has_variable_name(var_name)
        elif self.children[0].token_type == SYMBOLS.POW:
            return self.children[2].has_variable_name(var_name) or self.children[4].has_variable_name(var_name)

    def resolve(self):
        if self.children[0].token_type == SYMBOLS.L_PAREN:
            # FACTOR::=L_PAREN EXPR R_PAREN
            self.children[1] = self.children[1].resolve()
            return self
        elif self.children[0].token_type == SYMBOLS.OPERAND:
            # FACTOR::=OPERAND
            return self
        elif self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:
            # FACTOR::=S_PARAM_FUNC L_PAREN EXPR R_PAREN
            self.children[2] = self.children[2].resolve()
            return self
        elif self.children[0].token_type == SYMBOLS.LOG:
            # FACTOR::=LOG L_PAREN EXPR COMMA EXPR R_PAREN
            self.children[2] = self.children[2].resolve() # base_expr
            self.children[4] = self.children[4].resolve() # value_expr
            return self
        elif self.children[0].token_type == SYMBOLS.OPER_MIN:
            # FACTOR::=OPER_MIN OPERAND
            result = self.children[1].resolve()
            if result.is_zero(): # -0 case
                return FactorNode.from_number('0')
            else:
                self.children[1] = result
                return self
        elif self.children[0].token_type == SYMBOLS.POW:
            # FACTOR::=POW ( EXPR , EXPR )
            self.children[2] = self.children[2].resolve()  # base_expr
            self.children[4] = self.children[4].resolve()  # value_expr
            return self

    @classmethod
    def single_param_func_derivative(cls, func_name, in_func):
        if func_name == 'SIN':
            # sin(x)' -> cos(x)
            factor_node = FactorNode.make_s_param_func('cos', in_func)
            return factor_node
        elif func_name == 'COS':
            # cos(x)' -> -sin(x)
            factor_node = FactorNode.make_s_param_func('sin', in_func)
            expr_node = ExprNode.from_factor(factor_node)
            factor_node = FactorNode.make_min_factor(expr_node)
            return factor_node
        elif func_name == 'TAN':
            # tan(x)' -> sec(x)^2
            result = FactorNode.make_s_param_func('sec', in_func) # FACTOR
            result = ExprNode.from_factor(result)
            result = FactorNode.make_pow(result, ExprNode.from_number('2'))
            return result
        elif func_name == 'CSC':
            # csc(x)' -> -csc(x)cot(x)
            factor_node1 = FactorNode.make_s_param_func('csc', in_func)
            factor_node2 = FactorNode.make_s_param_func('cot', in_func)
            term_node = TermNode.from_factor(factor_node2)

            term_node2 = TermNode.from_factor(factor_node1, 'OPER_MULT', term_node)
            new_term_node = ExprNode.from_term(term_node2)
            new_factor_node = FactorNode.make_min_factor(new_term_node)
            return new_factor_node
        elif func_name == 'SEC':
            # sec(x)' -> sec(x)tan(x)
            factor_node1 = FactorNode.make_s_param_func('sec', in_func)
            factor_node2 = FactorNode.make_s_param_func('tan', in_func)
            term_node = TermNode.from_factor(factor_node2)

            term_node2 = TermNode.from_factor(factor_node1, 'OPER_MULT', term_node)
            expr_node = ExprNode.from_term(term_node2)
            new_factor_node = FactorNode.from_expr(expr_node)
            return new_factor_node
        elif func_name == 'COT':
            # cot(x)' -> -(csc(x)^2)
            factor_node = FactorNode.make_s_param_func('csc', in_func)

            result = TermNode.from_factor(factor_node)
            result = ExprNode.from_term(result)
            result = FactorNode.make_pow(result, ExprNode.from_number('2')) # csc(x) ^ 2
            result = ExprNode.from_factor(result)
            result = FactorNode.make_min_factor(result) # -(csc(x)^2)
            return result
        else:
            raise Exception('wrong function name')

    def to_string(self):
        if self.children[0].token_type == SYMBOLS.L_PAREN:
            # FACTOR::=L_PAREN EXPR R_PAREN
            return '(' + self.children[1].to_string() + ')'
        elif self.children[0].token_type == SYMBOLS.OPERAND:
            # FACTOR::=OPERAND
            return self.children[0].to_string()
        elif self.children[0].token_type == SYMBOLS.S_PARAM_FUNC:
            # FACTOR::=S_PARAM_FUNC L_PAREN EXPR R_PAREN
            return self.children[0].to_string() + '(' + self.children[2].to_string() + ')'
        elif self.children[0].token_type == SYMBOLS.LOG:
            # FACTOR::=LOG L_PAREN EXPR COMMA EXPR R_PAREN
            return 'log(' + self.children[2].to_string() + ',' + self.children[4].to_string() + ')'
        elif self.children[0].token_type == SYMBOLS.POW:
            # FACTOR::=POW ( EXPR , EXPR )
            return 'pow(' + self.children[2].to_string() + ',' + self.children[4].to_string() + ')'
        elif self.children[0].token_type == SYMBOLS.OPER_MIN:
            # FACTOR::=OPER_MIN OPERAND
            return '-' + self.children[1].to_string()


class OperandNode(CNode):
    def __init__(self, buf=None):
        super().__init__(SYMBOLS.OPERAND, buf)

    def evaluate(self, var_assign=None):
        return self.children[0].evaluate(var_assign)

    def derivative(self, variable_name):
        if self.children[0].token_type == SYMBOLS.VARIABLE and self.children[0].value == variable_name:
            operand_node = OperandNode('OPERAND')
            operand_node.children.append(NumberNode('1'))
            return operand_node
        else:
            operand_node = OperandNode('OPERAND')
            operand_node.children.append(NumberNode('0'))
            return operand_node

    def is_zero(self):
        return self.children[0].raw_string == '0' or self.children[0].raw_string == '0.0'

    def is_one(self):
        return self.children[0].raw_string == '1' or self.children[0].raw_string == '1.0'

    def is_const(self):
        return self.children[0].token_type == SYMBOLS.CONST

    def has_variable(self):
        return self.children[0].token_type == SYMBOLS.VARIABLE

    def has_variable_name(self, var_name):
        return self.children[0].token_type == SYMBOLS.VARIABLE and self.children[0].has_variable_name(var_name)

    @classmethod
    def from_elements(cls, val):
        operand_node = OperandNode('OPERAND')
        operand_node.children.append(val)
        return operand_node

    @classmethod
    def from_number(cls, num):
        if num == 'E' or num == 'PI':
            number = ConstNode.from_number(num)
        else:
            number = NumberNode(num)
        operand = OperandNode('OPERAND')
        operand.children.append(number)
        return operand

    @classmethod
    def from_variable(cls, var_name):
        operand_node = OperandNode('OPERAND')
        operand_node.children.append(VariableNode(var_name))
        return operand_node        

    def to_string(self):
        return self.children[0].to_string()


class SingleParamFuncionNode(CNode):
    def __init__(self, buf=None):
        s = ''.join(buf).lower()
        self.value = s
        if s == 'log':
            super().__init__(SYMBOLS.LOG, s)
        elif s == 'exp':
            super().__init__(SYMBOLS.EXP, s)
        elif s == 'sin':
            super().__init__(SYMBOLS.SIN, s)
        elif s == 'cos':
            super().__init__(SYMBOLS.COS, s)
        elif s == 'tan':
            super().__init__(SYMBOLS.TAN, s)
        elif s == 'csc':
            super().__init__(SYMBOLS.CSC, s)
        elif s == 'sec':
            super().__init__(SYMBOLS.SEC, s)
        elif s == 'cot':
            super().__init__(SYMBOLS.COT, s)
        elif s == 'pow':
            super().__init__(SYMBOLS.POW, s)
        else:
            super().__init__(SYMBOLS.S_PARAM_FUNC, s)

    def op(self):
        if self.children[0].token_type == SYMBOLS.EXP:
            return math.exp
        elif self.children[0].token_type == SYMBOLS.SIN:
            return math.sin
        elif self.children[0].token_type == SYMBOLS.COS:
            return math.cos
        elif self.children[0].token_type == SYMBOLS.TAN:
            return math.tan
        elif self.children[0].token_type == SYMBOLS.CSC:
            return lambda x: 1.0 / math.sin(x)
        elif self.children[0].token_type == SYMBOLS.SEC:
            return lambda x: 1.0 / math.cos(x)
        elif self.children[0].token_type == SYMBOLS.COT:
            return lambda x: 1.0 / math.tan(x)
        else:
            raise Exception('SingleParamFunc op - Unknown Operator')

    def to_string(self):
        return self.children[0].raw_string

########################################################################################################################

class ComputationalGraph(object):
    def __init__(self, root, var_names=None):
        self.root = root
        self.tmp = []
        self.var_names = var_names

    def __str__(self):
        return self.root.to_string()

    def __repr__(self):
        return self.__str__()

    def structure_string(self):
        return self.root.children_str()

    def derivative(self, var_name):
        d_root = self.root.derivative(var_name)
        return ComputationalGraph(d_root)

    # def derivative_all(self):
    #     d_graphs = []
    #
    #     for var_name in self.var_names:
    #         # TODO: fix var_names correctly
    #         d_root = self.root.derivative(var_name)
    #         d_var_names = set(self.var_names)
    #         d_var_names.remove(var_name)
    #         d_graph = ComputationalGraph(d_root, d_var_names)
    #         d_graphs.append(d_graph)
    #
    #     return d_graphs

    def evaluate(self, var_assign=None):
        return self.root.evaluate(var_assign)

    def is_differentiable(self, var_assign):
        epsilon = 1e-8
        threshold = 1e-5
        results = {}

        for k, v in var_assign.items():
            try:
                l_point = v - epsilon
                r_point = v + epsilon
                l_val = self.root.evaluate({k: l_point})
                r_val = self.root.evaluate({k: r_point})
                m_val = self.root.evaluate({k: v})
                l_lim = (l_val - m_val) / (-epsilon)
                r_lim = (r_val - m_val) / epsilon

                if l_lim - r_lim < threshold:
                    results[k] = True
                else:
                    results[k] = False
            except:
                results[k] = False
        return results

    def gradient(self, var_names=None):
        if var_names is None:
            var_names = self.var_names
        grad = {}
        for var_name in var_names:
            grad[var_name] = self.derivative(var_name)
        return grad

    def directional_derivative(self, s_point, d_point):
        '''
        s_point, d_point: dictionary type.
        Names in s_point and var_names have to be same.
        '''
        # assert len(s_point) == len(self.var_names)
        assert s_point.keys() == self.var_names

        diff = {}
        for k in s_point.keys():
            diff[k] = s_point[k] - d_point[k]
        s = sum(map(lambda k: diff[k] ** 2, diff))

        grad = self.gradient()
        for var_name in self.var_names:
            grad[var_name].root = grad[var_name].root.multiply(diff[var_name] / s)

        return grad

    def plot(self, label, var_name=None, min_range=-10, max_range=10, n_points=100, show=False):
        if len(self.var_names) == 2:
            fig = self.plot_3d(label,
                               min_range=min_range,
                               max_range=max_range,
                               n_points=n_points,
                               show=show,
                               save=True)
        else:
            fig = self.plot_through(var_name=var_name,
                                    min_range=min_range,
                                    max_range=max_range,
                                    n_points=n_points,
                                    label=label,
                                    show=show,
                                    save=True)
        return fig

    def plot_through(self, var_name, min_range=-10, max_range=10, n_points=100, label=None, fmt=None, save=False, show=False):
        x_range = np.linspace(min_range, max_range, n_points, endpoint=True)
        # y_values = [self.root.evaluate(var_assign={var_name: x_val}) for x_val in x_range]

        available_x_range = []
        y_values = []
        for x_val in x_range:
            try:
                if self.is_differentiable({var_name: x_val})[var_name]:
                    y_values.append(self.root.evaluate(var_assign={var_name: x_val}))
                    available_x_range.append(x_val)
                else:
                    y_values.append(np.nan)
                    available_x_range.append(x_val)
                # y_values.append(self.root.evaluate(var_assign={var_name: x_val}))
                # available_x_range.append(x_val)
            except ValueError:
                continue

        x_range = available_x_range

        fig = plt.figure()
        if fmt:
            handle, = plt.plot(x_range, y_values, fmt, label=label)
            # handle = plt.scatter(x_range, y_values, fmt, label=label)
        else:
            handle, = plt.plot(x_range, y_values, label=label)
            # handle = plt.scatter(x_range, y_values, label=label)

        plt.legend(handles=[handle], loc='upper right')

        if save:
            # file_name = str(datetime.datetime.now()).split(' ')[0]
            file_name = str(datetime.datetime.now()).replace(' ', '-').replace(':', '-').split('.')[0]
            plt.savefig(file_name + '.png')
            Image.open(file_name + '.png').save(file_name + '.jpg', 'JPEG')
            os.remove(file_name + '.png')

        if show:
            fig.show()

        return fig

    def resolve(self):
        c = copy.deepcopy(self.root)
        new_root = c.resolve()
        return ComputationalGraph(new_root)

    def plot_3d(self, label, min_range=-10, max_range=10, n_points=100, fmt=None, save=False, show=False):
        x_range = np.linspace(min_range, max_range, n_points, endpoint=True)
        y_range = np.linspace(min_range, max_range, n_points, endpoint=True)
        x, y = np.meshgrid(x_range, y_range)
        z = np.zeros(x.shape)
        s = x.shape

        for i in range(s[0]):
            for j in range(s[1]):
                z[i, j] = self.root.evaluate({'x': x[i,j], 'y': y[i,j]})

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        surf = ax.plot_surface(x, y, z, label=label)
        # plt.legend(handles=[surf])
        # ax.legend()

        # file_name = str(datetime.datetime.now()).split(' ')[0]
        file_name = str(datetime.datetime.now()).replace(' ', '-').replace(':', '-').split('.')[0]
        plt.savefig(file_name + '.png')
        Image.open(file_name + '.png').save(file_name + '.jpg', 'JPEG')
        os.remove(file_name + '.png')

        if show:
            fig.show()
            # plt.show()

        return fig

    def plot_3d_arrow(self, ax, x, y, z):
        u = np.sin(np.pi * x) * np.cos(np.pi * y) * np.cos(np.pi * z)
        v = -np.cos(np.pi * x) * np.sin(np.pi * y) * np.cos(np.pi * z)
        w = (np.sqrt(2.0 / 3.0) * np.cos(np.pi * x) * np.cos(np.pi * y) *
             np.sin(np.pi * z))

        # ax = fig.gca(projection='3d')
        q = ax.quiver(x, y, z, u, v, w, length=0.1, normalize=True, cmap='Reds', lw=2)
        q.set_array(np.random.rand(np.prod(x.shape)))

    @classmethod
    def parse(cls, formula_str, bnf_file_name=None):
        lex = lexer.Lexer()
        if bnf_file_name is None:
            bnf_file_name = 'bnf.txt'
        pars = parser.Parser(bnf_file_name)
        tokens = lex.tokenize(formula_str)
        graph = pars.parse(tokens)
        return graph

    def to_string(self):
        return self.root.to_string()

class NotSupportFunctionException(Exception):
    def __init__(self, message):
        self.message = message