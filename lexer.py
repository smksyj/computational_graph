from symbols import SYMBOLS
import computational_graph


class Lexer(object):
    def __init__(self):
        self.nums = '.0123456789'
        lower = range(ord('a'), ord('a') + 26)
        upper = range(ord('A'), ord('A') + 26)
        self.chars = list(map(chr, [*lower, *upper]))
        # self.operators = ['+', '-', '*', '/', '^']
        self.operators = ['+', '-', '*', '/']
        self.reserved_words = ['log', 'exp', 'sin', 'cos', 'tan', 'csc', 'sec', 'cot', 'pow']

    def tokenize(self, string):
        if len(string) == 0 or string == '\n':
            return []
        buf = []
        tokens = []
        string = string.replace(' ', '') + '\0'
        for c in string:
            if len(buf) == 0:
                if c == '(':
                    tokens.append(computational_graph.LParenNode(c))
                elif c == ')':
                    if self.is_number(tokens[-1]) or self.is_char(tokens[-1]) or \
                                    tokens[-1].token_type == SYMBOLS.R_PAREN:
                        tokens.append(computational_graph.RParenNode(c))
                    else:
                        raise Exception(') without (')
                elif c == '+':
                    tokens.append(computational_graph.PlusNode(c))
                elif c == '-':
                    tokens.append(computational_graph.MinusNode(c))
                elif c == '*':
                    tokens.append(computational_graph.MultNode(c))
                elif c == '/':
                    tokens.append(computational_graph.DivNode(c))
                elif c == '^':
                    tokens.append(computational_graph.PowNode(c))
                elif c == ',':
                    tokens.append(computational_graph.CommaNode(c))
                else:
                    buf.append(c)
            elif c == '\0':
                if self.is_number(buf[0]):
                    tokens.append(computational_graph.NumberNode(buf))
                    buf.clear()
                elif self.is_reserved_word(buf):
                    tokens.append(computational_graph.SingleParamFuncionNode(buf))
                    buf.clear()
                elif ''.join(buf) == 'E':
                    tokens.append(computational_graph.ENode(buf))
                    buf.clear()
                elif ''.join(buf) == 'PI':
                    tokens.append(computational_graph.PINode(buf))
                    buf.clear()
                else:
                    tokens.append(computational_graph.VariableNode(buf))
                    buf.clear()
            else:
                if c == '(':
                    if self.is_reserved_word(buf):
                        tokens.append(computational_graph.SingleParamFuncionNode(buf))
                        buf.clear()
                        tokens.append(computational_graph.LParenNode(c))
                    else:
                        raise Exception('L_PAREN without operator')
                elif c == ')':
                    if self.is_number(buf[0]):
                        tokens.append(computational_graph.NumberNode(buf))
                        buf.clear()
                    elif self.is_reserved_word(buf):
                        tokens.append(computational_graph.SingleParamFuncionNode(buf))
                        buf.clear()
                    elif ''.join(buf) == 'E':
                        tokens.append(computational_graph.ENode(buf))
                        buf.clear()
                    elif ''.join(buf) == 'PI':
                        tokens.append(computational_graph.PINode(buf))
                        buf.clear()
                    else:
                        tokens.append(computational_graph.VariableNode(buf))
                        buf.clear()
                    tokens.append(computational_graph.RParenNode(c))
                elif self.is_operator(c):
                    if self.is_number(buf[0]):
                        tokens.append(computational_graph.NumberNode(buf))
                        buf.clear()
                    elif ''.join(buf) == 'E':
                        tokens.append(computational_graph.ENode(buf))
                        buf.clear()
                    elif ''.join(buf) == 'PI':
                        tokens.append(computational_graph.PINode(buf))
                        buf.clear()
                    else:
                        tokens.append(computational_graph.VariableNode(buf))
                        buf.clear()
                    if c == '+':
                        tokens.append(computational_graph.PlusNode(c))
                    elif c == '-':
                        tokens.append(computational_graph.MinusNode(c))
                    elif c == '*':
                        tokens.append(computational_graph.MultNode(c))
                    elif c == '/':
                        tokens.append(computational_graph.DivNode(c))
                    elif c == '^':
                        tokens.append(computational_graph.PowNode(c))
                elif c == ',':
                    if self.is_number(buf[0]):
                        tokens.append(computational_graph.NumberNode(buf))
                        buf.clear()
                    elif ''.join(buf) == 'E':
                        tokens.append(computational_graph.ENode(buf))
                        buf.clear()
                    elif ''.join(buf) == 'PI':
                        tokens.append(computational_graph.PINode(buf))
                        buf.clear()
                    else:
                        tokens.append(computational_graph.VariableNode(buf))
                        buf.clear()
                    tokens.append(computational_graph.CommaNode(c))
                else:
                    buf.append(c)
        # print('lexer tokens: ', tokens)
        return tokens


    def is_number(self, c):
        if isinstance(c, computational_graph.CNode):
            return c.token_type == SYMBOLS.NUMBER
        return c in self.nums

    def is_char(self, c):
        if isinstance(c, computational_graph.CNode):
            return c.token_type == SYMBOLS.VARIABLE
        return c in self.chars

    def is_operator(self, c):
        if isinstance(c, computational_graph.CNode):
            return c.token_type == SYMBOLS.OPERATOR
        return c in self.operators

    def is_reserved_word(self, buf):
        s = ''.join(buf)
        return s in self.reserved_words

if __name__=='__main__':
    lexer = Lexer()
    lines = map(lambda line: line.replace('\n', ''), open('input.txt', 'r').readlines())
    for line in lines:
        if len(line) == 0 or line.startswith('#') or line == '\n':
            continue
        tokens = lexer.tokenize(line)
        print(tokens)
