def split_line(line):
    var_assign = {}
    der_names = []

    if ':' in line:
        s = line.split(':')
        line = s[0]
        for t in s[1].split(','):
            der_names.append(t)
    
    if '|' in line:
        s = line.split('|')
        line = s[0]
        for t in s[1].split(','):
            v_name, v_val = t.split('=')
            var_assign[v_name] = float(v_val)
            
    return line, var_assign, der_names

def invalid_line(line):
    return len(line) == 0 or line == '\n' or line.startswith('#')
